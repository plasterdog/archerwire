<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Capron_Manufacturing
 */

get_header(); ?>

<div class="standard-top" style="padding:.125em 0;">
<div class="clear"></div>	
</div><!--ends the four landing sections -->

<div id="content" class="site-content">
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php the_archive_title(  ); ?></h1>
				<div class="archive-description"><?php the_archive_description(); ?></div>
			</header><!-- .page-header -->
<hr/>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="entry-content">
		<h1><a href="<?php the_permalink(); ?>" rel="bookmark">	<?php the_title(); ?></a></h1>
				<div class="left-side">
					<!-- standard_hours -->
<?php if(get_field('standard_hours')) {?>
<li><strong>Hours: </strong><?php the_field('standard_hours'); ?></li>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('standard_hours')) {?>
		
<?php }?> <!-- ends the second outer condition -->

<!--job_location -->
<?php if(get_field('job_location')) {?>
<li><strong>Location: </strong><?php the_field('job_location'); ?></li>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('job_location')) {?>
		
<?php }?> <!-- ends the second outer condition -->


<!-- job_pay_range -->
<?php if(get_field('job_pay_range')) {?>
<li><strong>Compensation: </strong><?php the_field('job_pay_range'); ?></li>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('job_pay_range')) {?>
		
<?php }?> <!-- ends the second outer condition -->


<!-- job_contact_name -->
<?php if(get_field('job_contact_name')) {?>
	<li><strong>Contact: </strong><?php the_field('job_contact_name'); ?>
		<?php }?> <!-- ends the second outer condition -->
			<!-- job_contact_title -->
			<?php if(get_field('job_contact_title')) {?> \ <?php the_field('job_contact_title'); ?>
			<!-- job_contact_email_address -->
			<?php if(get_field('job_contact_email_address')) {?> : 
			<a href="mailto:<?php the_field('job_contact_email_address'); ?>?subject=<?php the_title( ); ?>"><?php the_field('job_contact_email_address'); ?></a>
			</li><?php } ?><!-- ends the first condition -->
			<?php if(!get_field('job_contact_email_address')) {?></li><?php }?> <!-- ends the second outer condition -->
			<?php } ?><!-- ends the first condition -->
			<?php if(!get_field('job_contact_title')) {?>
	<?php } ?><!-- ends the first condition -->
<?php if(!get_field('job_contact_name')) {?>
<?php }?> <!-- ends the second outer condition -->

				</div> 
 			


	<div class="right-side">
	
	<?php the_excerpt();?>
	<p align="right"><a href="<?php the_permalink(); ?>" rel="bookmark">see the entire job description</a></p>
	</div>
	</div><!-- .entry-content -->
	<hr/>
</article><!-- #post-<?php the_ID(); ?> -->

			<?php endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
