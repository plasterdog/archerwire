<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Capron_Manufacturing
 */

get_header(); ?>


<div class="standard-top" style="padding:.125em 0;">
<div class="clear"></div>	
</div><!--ends the four landing sections -->


<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
		while ( have_posts() ) : the_post(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php the_title( ); ?> 
			<!-- job_status -->
<?php if(get_field('job_status')) {?>
	- <?php the_field('job_status'); ?>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('job_status')) {?>
		
<?php }?> <!-- ends the second outer condition -->

		</h1>

		<div class="entry-meta">
	
		</div><!-- .entry-meta -->

	</header><!-- .entry-header -->

	<div class="entry-content">

<!-- standard_hours -->
<?php if(get_field('standard_hours')) {?>
<strong>Hours:</strong> <?php the_field('standard_hours'); ?><br/>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('standard_hours')) {?>
		
<?php }?> <!-- ends the second outer condition -->

<!--job_location -->
<?php if(get_field('job_location')) {?>
<strong>Location:</strong> <?php the_field('job_location'); ?><br/>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('job_location')) {?>
		
<?php }?> <!-- ends the second outer condition -->


<!-- job_pay_range -->
<?php if(get_field('job_pay_range')) {?>
<strong>Compensation:</strong> <?php the_field('job_pay_range'); ?><br/>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('job_pay_range')) {?>
		
<?php }?> <!-- ends the second outer condition -->

<hr/>

		<?php
			the_content();
		?>

<aside><?php the_field('eeoc_statement'); ?></aside>
		
<!-- job_contact_email_address -->
<?php if(get_field('job_contact_email_address')) {?>
	<h3>Please forward your resume and cover letter to:</h3>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('job_contact_email_address')) {?>
		
<?php }?> <!-- ends the second outer condition -->

<!-- job_contact_name -->
<?php if(get_field('job_contact_name')) {?>
	<h4><?php the_field('job_contact_name'); ?>
		<?php }?> <!-- ends the second outer condition -->
			<!-- job_contact_title -->
			<?php if(get_field('job_contact_title')) {?> \ <?php the_field('job_contact_title'); ?>
			<!-- job_contact_email_address -->
			<?php if(get_field('job_contact_email_address')) {?> : 
			<a href="mailto:<?php the_field('job_contact_email_address'); ?>?subject=<?php the_title( ); ?>"><?php the_field('job_contact_email_address'); ?></a>
			</h4><?php } ?><!-- ends the first condition -->
			<?php if(!get_field('job_contact_email_address')) {?></h3><?php }?> <!-- ends the second outer condition -->
			<?php } ?><!-- ends the first condition -->
			<?php if(!get_field('job_contact_title')) {?>
	</h4>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('job_contact_name')) {?>
<?php }?> <!-- ends the second outer condition -->



	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php capron_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
			
<?php 
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<aside id="secondary" class="widget-area">
<h2> Current Open Positions</h2>
<!-- https://www.lehelmatyus.com/958/wordpress-custom-taxonomy-query -->
<?php 
    // Query Arguments
    $args = array(
        'post_type' => 'job', // the post type
        'orderby' => 'title', // sort factor
        'order' => ASC, // sort order
        'tax_query' => array(
            array(
                'taxonomy' => 'status', // the custom vocabulary
                'field'    => 'slug',                 
                'terms'    => array('active'),      // provide the term slugs
            ),
        ),
    );

    // The query
    $the_query = new WP_Query( $args );

    // The Loop
    if ( $the_query->have_posts() ) {

        echo '<ul>';
        $html_list_items = '';
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            $html_list_items .= '<li>';
            $html_list_items .= '<a href="' . get_permalink() . '">';
            $html_list_items .= get_the_title();
            $html_list_items .= '</a>';
            $html_list_items .= '</li>';
        }
        echo $html_list_items;
        echo '</ul>';

    } else {
        // no posts found
    }

    wp_reset_postdata();

    ?>






</aside><!-- #secondary -->



<?php
get_footer();
