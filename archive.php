<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Capron_Manufacturing
 */

get_header(); ?>

<div class="standard-top" style="padding:.125em 0;">
<div class="clear"></div>	
</div><!--ends the four landing sections -->

<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->
<hr/>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="entry-content">

			<?php if ( get_the_post_thumbnail( $post_id ) != '' ) { ?>
				<div class="archive-thumb"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail(''); ?></a></div>
			<?php   } else { ?>
				<div class="archive-thumb"> <a href="<?php the_permalink(); ?>" rel="bookmark"> 
				<img src="<?php bloginfo('template_directory'); ?>/images/default.gif" alt="<?php the_title(); ?>" ></a></div> 
 			<?php    } ?>	

	<div class="archive-excerpt">
	<h1><a href="<?php the_permalink(); ?>" rel="bookmark">	<?php the_title(); ?></a></h1>
	<?php the_excerpt();?>
	<p align="right"><a href="<?php the_permalink(); ?>" rel="bookmark">read the entire article</a></p>
	</div>
	</div><!-- .entry-content -->
	<hr/>
</article><!-- #post-<?php the_ID(); ?> -->

			<?php endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
